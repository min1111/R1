import axios from "axios";

export const joinApi = axios.create({
  baseURL: "http://localhost:6112",
});

export let headers = {
  Authorization: `${
    localStorage.getItem("Authorization")
      ? localStorage.getItem("Authorization")
      : "Bearer"
  }`,
};

export let authApi = axios.create({
  baseURL: "http://localhost:6112",
  headers: headers,
});
